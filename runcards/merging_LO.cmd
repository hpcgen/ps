Tune:ee         = 7
Beams:frameType = 5

Main:numberOfEvents = 50000
#Next:numbercount = 10

# Switch off event generation steps
PartonLevel:MPI              = off

! Use same PDFs / alpha_s value as in ME calculation (not necessary!)
SpaceShower:alphaSvalue   = 0.118
TimeShower:alphaSvalue    = 0.118

! Wimpy shower
#TimeShower:pTmaxMatch       = 1
#SpaceShower:pTmaxMatch      = 1

! Specify merging parameters for CKKW-L, UMEPS, UNLOPS.
Merging:TMS                 = 20.                  ! merging scale value
Merging:Process             = pp>LEPTONS,NEUTRINOS ! process definition
Merging:nJetMax             = 2        ! maximal number of additional LO jets --- globally!!!
Merging:nJetMaxNLO          = 1        ! maximal number of additional LO jets --- globally!!!
! Factorisation/renormalisation scales in the 2->2 process
Merging:muFac               = 91.188
Merging:muRen               = 91.188
Merging:muFacInME           = 91.188
Merging:muRenInME           = 91.188
! Switches
Merging:enforceCutOnLHE     = off # check ME cuts before processing
Merging:applyVeto           = off # prevent py8 from sucking in events
Merging:includeWeightInXsection  = on # lass lieber aus, default is on i.e. merging weight is 1
Merging:unorderedASscalePrescrip = 0 # default is 1 --- 0 means use max scale for alphS for unordered
Merging:incompleteScalePrescrip  = 1 # Ne super Sache
Merging:mayRemoveDecayProducts   = off

Merging:kFactor1j = 1.2818103054504604
Merging:kFactor2j = 1.2818103054504604


! Do not include rapidity ordering (not necessary!)
SpaceShower:rapidityOrder = off

! Be more forgiving with momentum mismatches.
Check:epTolErr               = 2e-2

1:m0 = 0.0
2:m0 = 0.0
3:m0 = 0.0
4:m0 = 0.0
5:m0 = 0.0


Merging:doUNLOPSTree      = on
Merging:doUNLOPSSubt      = off
Merging:doUNLOPSLoop      = off
Merging:doUNLOPSSubtNLO   = off

