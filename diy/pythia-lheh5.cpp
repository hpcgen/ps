
#include <cmath>
#include <vector>
#include <array>
#include <algorithm>
#include <functional>
#include <numeric>
#include <stdexcept>
#include <random>
#include <typeinfo>

#include <diy/master.hpp>
#include <diy/reduce.hpp>
#include <diy/partners/merge.hpp>
#include <diy/decomposition.hpp>
#include <diy/assigner.hpp>
#include <diy/mpi.hpp>
#include <diy/serialization.hpp>
#include <diy/partners/broadcast.hpp>
#include <diy/reduce-operations.hpp>

#include "PtjTMSdefinitionHooks.h" // Stefan's unlops hook

#include "config.hpp"
#include "GenericBlock.hpp"
#include "Reduce.hpp"
#include "Tools.hpp"
#include "CalcConfig.hpp"
#include "Serialisation.hpp"

#include "YODA/ReaderYODA.h"
#include "YODA/WriterYODA.h"
#include "YODA/AnalysisObject.h"

#include "Pythia8/Pythia.h"
#include "Pythia8/LHEF3.h"
#include "Pythia8Plugins/HepMC2.h"
#include "Rivet/AnalysisHandler.hh"
#undef foreach // This line prevents a clash of definitions of rivet's legacy foreach with that of DIY

#include "HepMC/IO_GenEvent.h"

#include "fastjet/ClusterSequence.hh" // This is to quieten fastjet

using namespace std;
using namespace Pythia8;

#include <highfive/H5File.hpp>
#include <highfive/H5FileDriver.hpp>
#include <highfive/H5DataSet.hpp>

namespace LHEH5 {

  struct ProcInfo {
    int pid, nplo, npnlo;
    double unitwgt, xsec, error;
    inline ProcInfo(int _pid,int _nplo,int _npnlo,
		    double _unitwgt,double _xsec,double _error):
      pid(_pid), nplo(_nplo), npnlo(_npnlo),
      unitwgt(_unitwgt), xsec(_xsec), error(_error) {}
  };//end of struct ProcInfo

  std::ostream &operator<<(std::ostream &s,const ProcInfo &p)
  { return s<<"[pid="<<p.pid<<",nplo="<<p.nplo<<",npnlo="<<p.npnlo
	    <<",unitwgt="<<p.unitwgt<<",xsec="<<p.xsec<<"]"; }

  struct Particle {
    int id, st, mo1, mo2, cl1, cl2;
    double px, py, pz, e, m, lt, sp;
    inline Particle(int _id,int _st,int _mo1,int _mo2,int _cl1,int _cl2,
		    double _px,double _py,double _pz,double _e,double _m,
		    double _lt,double _sp):
      id(_id), st(_st), mo1(_mo1), mo2(_mo2), cl1(_cl1), cl2(_cl2),
      px(_px), py(_py), pz(_pz), e(_e), m(_m), lt(_lt), sp(_sp) {}
  };// end of struct Particle

  std::ostream &operator<<(std::ostream &s,const Particle &p)
  { return s<<"{id="<<p.id<<",st="<<p.st
	    <<",mo=["<<p.mo1<<","<<p.mo2<<"]"
	    <<",cl=["<<p.cl1<<","<<p.cl2<<"]"
	    <<",p=("<<p.e<<","<<p.px<<","<<p.py<<","<<p.pz<<")}"; }

  struct Event: public std::vector<Particle> {
    ProcInfo pinfo;
    size_t trials;
    std::vector<double> wgts;
    double mur, muf, muq, aqed, aqcd;
    inline Event(const ProcInfo &_pinfo,
		 size_t _trials,std::vector<double> _wgts,
		 double _mur,double _muf,double _muq,
		 double _aqed,double _aqcd):
      pinfo(_pinfo), trials(_trials), wgts(_wgts),
      mur(_mur), muf(_muf), muq(_muq), aqed(_aqed), aqcd(_aqcd) {}
  };// end of struct Event

  std::ostream &operator<<(std::ostream &s,const Event &e)
  { s<<"Event "<<e.pinfo<<" {\n"
     <<"  trials="<<e.trials<<",weights=("<<e.wgts[0];
    for (size_t i(1);i<e.wgts.size();++i) s<<","<<e.wgts[i];
    s<<")\n  mur="<<e.mur<<", muf="<<e.muf<<", muq="<<e.muq
     <<",aqed="<<e.aqed<<",aqcd="<<e.aqcd<<"\n";
    for (size_t i(0);i<e.size();++i) s<<"  "<<e[i]<<"\n";
    return s<<"}"; }

  class LHEFile {
  private:
    
    std::vector<int> version;
    std::vector<std::vector<double> > evts, parts, pinfo;
    std::vector<std::string> wgtnames;

    inline Particle GetParticle(size_t i) const
    {
      return Particle(parts[i][0],parts[i][1],parts[i][2],parts[i][3],
		      parts[i][4],parts[i][5],parts[i][6],parts[i][7],
		      parts[i][8],parts[i][9],parts[i][10],
		      parts[i][11],parts[i][12]);
    }

  public:

    inline double TotalXS() const
    {
      double xs(0.);
      for (int i(0);i<pinfo.size();++i) xs+=pinfo[i][3];
      return xs;
    }

    inline const std::vector<std::string> &
    WeightNames() const { return wgtnames; }

    inline size_t NProcesses() const { return pinfo.size(); }
    inline ProcInfo GetProcInfo(const size_t pid) const
    {
      return ProcInfo(pid,pinfo[pid][1],pinfo[pid][2],
		      pinfo[pid][5],pinfo[pid][3],pinfo[pid][4]);
    }

    inline size_t NEvents() const { return evts.size(); }
    inline Event GetEvent(size_t i) const
    {
      std::vector<double> wgts(evts[i].begin()+9,evts[i].end());
      Event e(GetProcInfo(evts[i][0]?evts[i][0]-1:0),evts[i][3],wgts,
	      evts[i][6],evts[i][5],evts[i][4],
	      evts[i][7],evts[i][8]);
      for (int n(0);n<evts[i][1];++n)
	e.push_back(GetParticle(evts[i][2]-evts[0][2]+n));
      return e;
    }
 
    void ReadHeader(HighFive::File &file)
    {
      file.getDataSet("version").read(version);
      file.getDataSet("procInfo").read(pinfo);
      HighFive::DataSet events(file.getDataSet("events"));
      auto attr_keys(events.listAttributeNames());
      HighFive::Attribute a(events.getAttribute(attr_keys[0]));
      a.read(wgtnames);
      for (int i(0);i<9;++i) wgtnames.erase(wgtnames.begin());
    }
    void ReadEvents(HighFive::File &file,size_t first,size_t n_events)
    {
      HighFive::DataSet events(file.getDataSet("events"));
      std::vector<size_t> eoffsets{first,0};
      std::vector<size_t> ecounts{n_events,9+wgtnames.size()};
      events.select(eoffsets,ecounts).read(evts);
      HighFive::DataSet particles(file.getDataSet("particles"));
      std::vector<size_t> poffsets{(size_t)evts.front()[2],0};
      size_t nps(evts.back()[2]-evts.front()[2]+evts.back()[1]);
      std::vector<size_t> pcounts{nps,13};
      particles.select(poffsets,pcounts).read(parts);
    }
    
  };// end of struct LHEFile

}// end of namespace LHEH5

using namespace LHEH5;

class LHAupH5 : public Pythia8::LHAup {
public:

  LHAupH5(HighFive::File* file, LHEH5::LHEFile *lhef,
	  size_t firstEvent, size_t readSize, bool verbose=false):
    _lhef(lhef), _numberRead(0), _nTrials(0)
  {
    _lhef->ReadEvents(*file,firstEvent,readSize);
    // This reads the init information
    _weightnames=_lhef->WeightNames();
    std::vector<double> info;
    file->getDataSet("init").read(info);
    setBeamA(info[0],info[2],info[4],info[6]);
    setBeamB(info[1],info[3],info[5],info[7]);
    int weightingStrategy=info[8];
    setStrategy(-4);
    int numProcesses=info[9];
    vector<int> procId(numProcesses);
    vector<double> xSection(numProcesses);
    vector<double> error(numProcesses);
    vector<double> unitWeight(numProcesses);
    for (size_t i(0);i<numProcesses;++i) {
      ProcInfo pi(_lhef->GetProcInfo(i));
      procId[i]=pi.pid;
      xSection[i]=pi.xsec;
      error[i]=pi.error;
      unitWeight[i]=pi.unitwgt;
    }
    for (size_t np=0; np<numProcesses;++np) {
      addProcess(procId[np],xSection[np],error[np],unitWeight[np]);
      xSecSumSave+=xSection[np];
      xErrSumSave+=pow2(error[np]);
    }
  }

  bool setInit() override { return true; }
  
  bool setEvent(int idProc=0) override
  {
    LHEH5::Event evt(_lhef->GetEvent(_numberRead));
    if (evt[0].pz<0 && evt[1].pz>0) std::swap<LHEH5::Particle>(evt[0],evt[1]);
    setProcess(evt.pinfo.pid,evt.wgts[0],evt.mur,evt.aqed,evt.aqcd);
    nupSave    = evt.size();
    idprupSave = evt.pinfo.pid;
    xwgtupSave = evt.wgts[0];
    scalupSave = evt.mur;
    aqedupSave = evt.aqed;
    aqcdupSave = evt.aqcd;
    double scalein = -1.;
    _eventweightvalues=evt.wgts;
    infoPtr->weights_compressed_names = &_weightnames;
    infoPtr->weights_compressed = &_eventweightvalues;
    for (unsigned int ip=0;ip<evt.size(); ++ip) {
      const LHEH5::Particle &p=evt[ip];
      if (ip < 2) addParticle(p.id,p.st,0, 0,p.cl1,p.cl2,
			      p.px,p.py,p.pz,p.e,p.m,p.lt,p.sp,scalein);
      else addParticle(p.id,p.st,p.mo1,p.mo2,p.cl1,p.cl2,
		       p.px,p.py,p.pz,p.e,p.m,p.lt,p.sp,scalein);
    }
    // Scale setting
    scalesNow.clear();
    scalesNow.muf   = evt.muf;
    scalesNow.mur   = evt.mur;
    scalesNow.mups  = evt.muq;
    infoPtr->scales = &scalesNow;
    infoPtr->setEventAttribute("npLO",std::to_string(evt.pinfo.nplo));
    infoPtr->setEventAttribute("npNLO",std::to_string(evt.pinfo.npnlo));
    _numberRead++;
    _nTrials+=evt.trials;
    return true;
  }

  size_t nTrials() { return _nTrials; }
  size_t nRead() { return _numberRead; }

private:

  LHEH5::LHEFile *_lhef;
  size_t _numberRead, _nTrials;
  vector<string> _weightnames;
  vector<double> _eventweightvalues;
  LHAscales scalesNow;

};// end of class LHAupH5



#include "opts.h"

typedef diy::DiscreteBounds Bounds;
typedef diy::RegularGridLink RCLink;

typedef GenericBlock<Bounds, PointConfig, AnalysisObjects> Block;
typedef ConfigBlockAdder<Bounds, RCLink, Block> AddBlock;

void print_block(Block* b,                             // local block
                 const diy::Master::ProxyWithLink& cp, // communication proxy
                 bool verbose)                         // user-defined additional arguments
{
   for (auto s : b->state.conf) {
      fmt::print(stderr, "[{}]: {}\n", cp.gid(), s);
   }
   fmt::print(stderr, "\n");
   for (auto s : b->state.analyses) {
      fmt::print(stderr, "[{}]: {} ", cp.gid(), s);
   }
   fmt::print(stderr, "\n");
}
void process_block_lhe_performance_same(Block* b, diy::Master::ProxyWithLink const& cp, int size, int rank,  bool verbose, int npc, string in_file, int nMax)
{
  // Minimise pythia's output
  //b->pythia.readString("Print:quiet = on");

  // Configure pythia with a vector of strings
  for (auto s  : b->state.conf) b->pythia.readString(s);
  // Py8 random seed for this block read from point config
  b->pythia.readString("Random:setSeed = on");
  b->pythia.readString("Random:seed = " + std::to_string(b->state.seed));
  // TODO seed mode 3, i.e. draw nrank random numbers

  HighFive::File file(in_file,HighFive::File::ReadOnly);
  size_t nEvents=file.getDataSet("events").
    getSpace().getDimensions().front();
  LHEH5::LHEFile *ef(new LHEH5::LHEFile());
  ef->ReadHeader(file);
  size_t eventOffset = 0;
  
  // Create an LHAup object that can access relevant information in pythia.
  LHAupH5* LHAup = new LHAupH5( &file, ef , eventOffset, nMax, verbose);
  b->lha = LHAup;

  b->pythia.settings.mode("Beams:frameType", 5);
  // Give the external reader to Pythia
  b->pythia.setLHAupPtr(LHAup);

   // hier unlops zeug
   // Allow to set the number of addtional partons dynamically. TODO here important
  int scheme = ( b->pythia.settings.flag("Merging:doUMEPSTree")
              || b->pythia.settings.flag("Merging:doUMEPSSubt")) ?
              1 :
               ( ( b->pythia.settings.flag("Merging:doUNLOPSTree")
              || b->pythia.settings.flag("Merging:doUNLOPSSubt")
              || b->pythia.settings.flag("Merging:doUNLOPSLoop")
              || b->pythia.settings.flag("Merging:doUNLOPSSubtNLO")) ?
              2 :
              0 );
  HardProcessBookkeeping* hardProcessBookkeepingPtr
    = new HardProcessBookkeeping(scheme);

  b->pythia.setUserHooksPtr(hardProcessBookkeepingPtr);

  double sigmaTotal  = 0.;
  double errorTotal  = 0.;
  double xs = 0.;
  for (int i=0; i < b->pythia.info.nProcessesLHEF(); ++i)
    xs += b->pythia.info.sigmaLHEF(i);
  if (verbose) fmt::print(stderr, "[{}] xs: {}\n", cp.gid(), xs);
  double sigmaSample = 0., errorSample = 0.; // NOTE in Stefan's unlops this is reset for each to be merged multi

  b->pythia.readString("Merging:unlopsTMSdefinition = 1");
  int unlopsType = b->pythia.settings.mode("Merging:unlopsTMSdefinition");

   MergingHooks* ptjTMSdefinitionPtr = (unlopsType<0)
    ? NULL
    : new PtjTMSdefinitionHooks(b->pythia.parm("Merging:TMS"),6.0);
  if (unlopsType >0) b->pythia.setMergingHooksPtr( ptjTMSdefinitionPtr );

  // All configurations done, initialise Pythia
  b->pythia.init();


  if (verbose) fmt::print(stderr, "[{}] starting event loop\n", cp.gid());
  // The event loop
  int nAbort = 5;
  int iAbort = 0;
  if (verbose)  fmt::print(stderr, "[{}] generating  {} events\n", cp.gid(),  ef->NEvents());
  for (size_t iEvent = 0; iEvent < nMax; ++iEvent) {
    if (verbose) fmt::print(stderr, "[{}] is at event {}\n", cp.gid(), iEvent);
    if (!b->pythia.next()) {
       // Gracefully ignore events with 0 weight
       if (LHAup->weight() == 0) {
          if (verbose) fmt::print(stderr, "[{}] encounters and ignores event {} as it has zero weight\n", cp.gid(), iEvent);
          continue;
       }
       else {
          if (++iAbort < nAbort) continue; // All other errors contribute to the abort counter
       }
      break;
    }
    //LHAup->listEvent();
    if (verbose ) LHAup->listEvent();
  }

  delete hardProcessBookkeepingPtr;
  if (unlopsType>0) delete ptjTMSdefinitionPtr;
}
void process_block_lhe_performance(Block* b, diy::Master::ProxyWithLink const& cp, int size, int rank,  bool verbose, int npc, string in_file, int nMax)
{
  // Minimise pythia's output
  b->pythia.readString("Print:quiet = on");

  // Configure pythia with a vector of strings
  for (auto s  : b->state.conf) b->pythia.readString(s);
  // Py8 random seed for this block read from point config
  b->pythia.readString("Random:setSeed = on");
  b->pythia.readString("Random:seed = " + std::to_string(b->state.seed+cp.gid()));
  // TODO seed mode 3, i.e. draw nrank random numbers

  HighFive::File file(in_file, HighFive::File::ReadOnly);  
  size_t nEvents=file.getDataSet("events").
    getSpace().getDimensions().front();
  LHEH5::LHEFile *ef(new LHEH5::LHEFile());
  ef->ReadHeader(file);
  if (nMax > 0 && nMax < nEvents) {
     nEvents = nMax;
  }
  size_t ev_rank = floor(nEvents/size);
  size_t eventOffset = rank*ev_rank;
  
  // Create an LHAup object that can access relevant information in pythia.
  LHAupH5* LHAup = new LHAupH5( &file, ef , eventOffset, ev_rank, verbose);
  b->lha = LHAup;

  b->pythia.settings.mode("Beams:frameType", 5);
  // Give the external reader to Pythia
  b->pythia.setLHAupPtr(LHAup);

   // hier unlops zeug
   // Allow to set the number of addtional partons dynamically. TODO here important
  HardProcessBookkeeping* hardProcessBookkeeping = NULL;
  int scheme = ( b->pythia.settings.flag("Merging:doUMEPSTree")
              || b->pythia.settings.flag("Merging:doUMEPSSubt")) ?
              1 :
               ( ( b->pythia.settings.flag("Merging:doUNLOPSTree")
              || b->pythia.settings.flag("Merging:doUNLOPSSubt")
              || b->pythia.settings.flag("Merging:doUNLOPSLoop")
              || b->pythia.settings.flag("Merging:doUNLOPSSubtNLO")) ?
              2 :
              0 );
  HardProcessBookkeeping* hardProcessBookkeepingPtr
    = new HardProcessBookkeeping(scheme);

  b->pythia.setUserHooksPtr(hardProcessBookkeepingPtr);

  double sigmaTotal  = 0.;
  double errorTotal  = 0.;
  double xs = 0.;
  for (int i=0; i < b->pythia.info.nProcessesLHEF(); ++i)
    xs += b->pythia.info.sigmaLHEF(i);
  if (verbose) fmt::print(stderr, "[{}] xs: {}\n", cp.gid(), xs);
  double sigmaSample = 0., errorSample = 0.; // NOTE in Stefan's unlops this is reset for each to be merged multi

  b->pythia.readString("Merging:unlopsTMSdefinition = 1");
  int unlopsType = b->pythia.settings.mode("Merging:unlopsTMSdefinition");

   MergingHooks* ptjTMSdefinitionPtr = (unlopsType<0)
    ? NULL
    : new PtjTMSdefinitionHooks(b->pythia.parm("Merging:TMS"),6.0);
  if (unlopsType >0) b->pythia.setMergingHooksPtr( ptjTMSdefinitionPtr );

  // All configurations done, initialise Pythia
  b->pythia.init();


  if (verbose) fmt::print(stderr, "[{}] starting event loop\n", cp.gid());
  // The event loop
  int nAbort = 5;
  int iAbort = 0;
  if (verbose)  fmt::print(stderr, "[{}] generating  {} events\n", cp.gid(),  ef->NEvents());
  for (size_t iEvent = 0; iEvent < ef->NEvents(); ++iEvent) {
    if (verbose) fmt::print(stderr, "[{}] is at event {}\n", cp.gid(), iEvent);
    if (!b->pythia.next()) {
       // Gracefully ignore events with 0 weight
       if (LHAup->weight() == 0) {
          if (verbose) fmt::print(stderr, "[{}] encounters and ignores event {} as it has zero weight\n", cp.gid(), iEvent);
          continue;
       }
       else {
          if (++iAbort < nAbort) continue; // All other errors contribute to the abort counter
       }
      break;
    }
    if (verbose ) LHAup->listEvent();
  }

  delete hardProcessBookkeepingPtr;
  if (unlopsType>0) delete ptjTMSdefinitionPtr;
}

//void process_block(Block* b, diy::Master::ProxyWithLink const& cp, int rank, std::vector<std::string> physConfig, std::vector<std::string> analyses, bool verbose)
void process_block_lhe(Block* b, diy::Master::ProxyWithLink const& cp, int size, int rank,  bool verbose, int npc, string in_file, int nMax)
{
   // TODO does that work???
   //if (cp.gid()>0)  {
      //fastjet::ClusterSequence ___;
      //___.set_fastjet_banner_stream(0);
   //}
  // This makes rivet only report ERRORs
  // TODO: can we have a global flag to steer verbosity of all moving parts?
  if (!verbose) Rivet::Log::setLevel("Rivet", Rivet::Log::WARNING);

  // Minimise pythia's output
  if (verbose) b->pythia.readString("Print:quiet = off");
  else b->pythia.readString("Print:quiet = on");

  // Configure pythia with a vector of strings
  for (auto s  : b->state.conf) b->pythia.readString(s);
  // Py8 random seed for this block read from point config
  b->pythia.readString("Random:setSeed = on");
  b->pythia.readString("Random:seed = " + std::to_string(b->state.seed+cp.gid()));
  // TODO seed mode 3, i.e. draw nrank random numbers

  HighFive::File file(in_file,HighFive::File::ReadOnly);
  size_t nEvents=file.getDataSet("events").
    getSpace().getDimensions().front();
  LHEH5::LHEFile *ef(new LHEH5::LHEFile());
  ef->ReadHeader(file);
  if (nMax > 0 && nMax < nEvents) nEvents = nMax;
  size_t ev_rank = floor(nEvents/size); // Total number of events this block/rank processes
  size_t eventOffset = rank*ev_rank; //

  fmt::print(stderr, "[{}] reads {}/{} events starting at {}\n", cp.gid(), ev_rank, nEvents, eventOffset);

  b->pythia.settings.mode("Beams:frameType", 5);
  // Create an LHAup object that can access relevant information in pythia.
  //LHAupH5* LHAup = new LHAupH5( &file , eventOffset, ev_rank, nEvents, verbose);
  //// Give the external reader to Pythia
  //b->pythia.setLHAupPtr(LHAup);

   // hier unlops zeug
   // Allow to set the number of addtional partons dynamically. TODO here important
  HardProcessBookkeeping* hardProcessBookkeeping = NULL;
  int scheme = ( b->pythia.settings.flag("Merging:doUMEPSTree")
              || b->pythia.settings.flag("Merging:doUMEPSSubt")) ?
              1 :
               ( ( b->pythia.settings.flag("Merging:doUNLOPSTree")
              || b->pythia.settings.flag("Merging:doUNLOPSSubt")
              || b->pythia.settings.flag("Merging:doUNLOPSLoop")
              || b->pythia.settings.flag("Merging:doUNLOPSSubtNLO")) ?
              2 :
              0 );
  HardProcessBookkeeping* hardProcessBookkeepingPtr
    = new HardProcessBookkeeping(scheme);

  if (scheme!=0) b->pythia.setUserHooksPtr(hardProcessBookkeepingPtr);

  double sigmaTotal  = 0.;
  double errorTotal  = 0.;
  double xs = 0.;
  for (int i=0; i < b->pythia.info.nProcessesLHEF(); ++i)
    xs += b->pythia.info.sigmaLHEF(i);
  if (verbose) fmt::print(stderr, "[{}] xs: {}\n", cp.gid(), xs);
  double sigmaSample = 0., errorSample = 0.; // NOTE in Stefan's unlops this is reset for each to be merged multi

  b->pythia.readString("Merging:unlopsTMSdefinition = 1");
  int unlopsType = b->pythia.settings.mode("Merging:unlopsTMSdefinition");

   MergingHooks* ptjTMSdefinitionPtr = (unlopsType<0)
    ? NULL
    : new PtjTMSdefinitionHooks(b->pythia.parm("Merging:TMS"),6.0);
  if (unlopsType >0) b->pythia.setMergingHooksPtr( ptjTMSdefinitionPtr );


  // Delete the AnalysisHandlerPtr to ensure there is no memory confusion
  if (b->ah) delete b->ah;
  
  b->ah = new Rivet::AnalysisHandler;
  b->ah->setIgnoreBeams();

  // Add all analyses to rivet
  // TODO: we may want to feed the "ignore beams" switch as well
  for (auto a : b->state.analyses) {
     b->ah->addAnalysis(a);
     if (verbose) fmt::print(stderr, "[{}] add  ######## analysis {}\n", cp.gid(), a);
  }

  if (verbose) fmt::print(stderr, "[{}] starting event loop\n", cp.gid());
  // The event loop
  int nAbort = 5;
  int iAbort = 0;

  // Create an LHAup object that can access relevant information in pythia.
  LHAupH5* LHAup = new LHAupH5( &file , ef, eventOffset, ev_rank, verbose);
  b->lha = LHAup;
  // Give the external reader to Pythia
  b->pythia.setLHAupPtr(LHAup);
  // All configurations done, initialise Pythia
  b->pythia.init();

  for (size_t iEvent = eventOffset; iEvent < eventOffset+ev_rank; ++iEvent) {
    if (verbose) fmt::print(stderr, "[{}] is at event {}\n", cp.gid(), iEvent);
    if (!b->pythia.next()) {
      if (verbose) b->pythia.stat();
      if (verbose) LHAup->listEvent();

      // Gracefully ignore events with 0 weight
      if (LHAup->weight() == 0) {
	if (verbose) fmt::print(stderr, "[{}] encounters and ignores event {} as it has zero weight\n", cp.gid(), iEvent);
	continue;
      }
      else {
	if (++iAbort < nAbort) continue; // All other errors contribute to the abort counter
      }
      break;
    }
    //if (verbose && iEvent < 2 ) LHAup->listEvent();
    if (verbose ) LHAup->listEvent();
    if (verbose) fmt::print(stderr, "[{}] event weight {} \n", cp.gid(), b->pythia.info.weight());
    // Get event weight(s).
    double evtweight         = b->pythia.info.weight();
    // Additional PDF/alphaS weight for internal merging.
    //evtweight               *= b->pythia.info.mergingWeightNLO() // commented out
    // Additional weight due to random choice of reclustered/non-reclustered
    // treatment. Also contains additional sign for subtractive samples.
    //*hardProcessBookkeepingPtr->getNormFactor(); // NOTE this would be necessary for NLO
    if (verbose) fmt::print(stderr, "[{}] after weight {} \n", cp.gid(), evtweight);
    HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
      
    // weight push_back
    // Work with weighted (LHA strategy=-4) events.
    //double normhepmc = 1.;
    //if (abs(b->pythia.info.lhaStrategy()) == 4)  // Triggering in l 128
    ////normhepmc = 1. / double(1e9*nEvent);
    //normhepmc = 1. / double(LHAup->nTrials());
    //// Work with unweighted events.
    //else
    ////normhepmc = xs / double(1e9*nEvent);
    //normhepmc = xs / double(LHAup->nTrials());
    double normhepmc = 1.;// / double(LHAup->nTrials()); //

    hepmcevt->weights().push_back(evtweight*normhepmc);
    if (verbose) fmt::print(stderr, "[{}] norm weight {} \n", cp.gid(), evtweight*normhepmc);
    b->ToHepMC.set_print_inconsistency(false);
    b->ToHepMC.set_free_parton_exception(false);
    b->ToHepMC.fill_next_event( b->pythia, hepmcevt );
    sigmaTotal  += evtweight*normhepmc;
    sigmaSample += evtweight*normhepmc;
    errorTotal  += pow2(evtweight*normhepmc);
    errorSample += pow2(evtweight*normhepmc);
    // Report cross section to hepmc
    HepMC::GenCrossSection xsec;
    xsec.set_cross_section( sigmaTotal, b->pythia.info.sigmaErr() );
    hepmcevt->set_cross_section( xsec );
    if (verbose) fmt::print(stderr, "[{}] xsec {} \n", cp.gid(), sigmaTotal);

    // Here more
    try {b->ah->analyze( *hepmcevt ) ;} catch (const std::exception& e)
      {
	if (verbose) fmt::print(stderr, "[{}] exception in analyze: {}\n", cp.gid(), e.what());
      }
    delete hepmcevt;
    // TODO: make 1000 a free parameter a la msg_every
    if (iEvent%1000 == 0 && cp.gid()==0) {
      if (b->state.num_events <0 | b->state.num_events > ef->NEvents()) {
	fmt::print(stderr, "[{}]  {}/{} \n", cp.gid(),  iEvent, ev_rank);//LHAup->getSize());
      }
      else {
	fmt::print(stderr, "[{}]  {}/{} \n", cp.gid(),  iEvent, ev_rank);//, b->state.num_events);
      }
    }
  }

  // Event loop is done, set xsection correctly and normalise histos
  // TODO: check that this setting of the xs is really correct
  b->pythia.stat();
  b->ah->setCrossSection( sigmaTotal);//b->pythia.info.sigmaGen() * 1.0E9);
  b->ah->finalize();

  // Push histos into block
  b->data = b->ah->getData();
  // Debug write out --- uncomment to write each block's YODA file
  //b->ah->writeData(std::to_string((1+npc)*(b->state.seed+cp.gid()))+".yoda");

  // Clean-up TODO important
  delete hardProcessBookkeepingPtr;
  if (unlopsType>0) delete ptjTMSdefinitionPtr;
}


void write_yoda(Block* b, diy::Master::ProxyWithLink const& cp, int rank, bool verbose)
{
 if (verbose) fmt::print(stderr, "[{}] -- rank {} sees write_yoda \n", cp.gid(), rank);
 double nTrials=b->lha->nTrials();
 double nRead=b->lha->nRead();
 diy::mpi::communicator world;
 MPI_Allreduce(MPI_IN_PLACE,&nTrials,1,MPI_DOUBLE,MPI_SUM,world);
 MPI_Allreduce(MPI_IN_PLACE,&nRead,1,MPI_DOUBLE,MPI_SUM,world);
  if (rank==0 && cp.gid()==0) {
    fmt::print("[{}] ME efficiency {}/{} = {}\n",cp.gid(),nRead,nTrials,nRead/nTrials);
    for (auto ao : b->buffer) {
      double sc = nTrials;
      if (ao->type()=="Histo1D") {
	dynamic_cast<YODA::Histo1D&>(*ao).scaleW(1./sc);
      }
      else if (ao->type()=="Histo2D") {
           dynamic_cast<YODA::Histo2D&>(*ao).scaleW(1./sc);
      }
    }
    if (verbose) fmt::print(stderr, "[{}] -- writing to file {}  \n", cp.gid(), b->state.f_out);
    YODA::WriterYODA::write(b->state.f_out, b->buffer);
  }
}

void clear_buffer(Block* b, diy::Master::ProxyWithLink const& cp, bool verbose)
{
  if (verbose) fmt::print(stderr, "[{}] -- clear buffer  \n", cp.gid());
  b->buffer.clear();
}

void set_pc(Block* b,                             // local block
                 const diy::Master::ProxyWithLink& cp, // communication proxy
                 PointConfig pc)                         // user-defined additional arguments
{
  if (cp.gid() == 0) b->state = pc;
}

inline bool file_exists (const std::string& name) {
    ifstream f(name.c_str());
    return f.good();
}

// --- main program ---//
int main(int argc, char* argv[])
{
    diy::mpi::environment env(argc, argv);
    diy::mpi::communicator world;

    size_t nBlocks = 0;
    int threads = 1;
    int runmode = 0;
    int nEvents=1000;
    size_t seed=1234;
    vector<std::string> analyses;
    std::string out_file="diy.yoda";
    std::string pfile="runPythia.cmd";
    std::string in_file="test.h5";
    std::string indir="";
    // get command line arguments
    using namespace opts;
    Options ops(argc, argv);
    bool                      verbose     = ops >> Present('v',
                                                           "verbose",
                                                           "verbose output");
    ops >> Option('t', "thread",    threads,   "Number of threads");
    ops >> Option('n', "nevents",   nEvents,   "Number of events to generate in total");
    ops >> Option('m', "runmode",   runmode,   "Runmode - 0 normal, 1 means performance");
    ops >> Option('b', "nblocks",   nBlocks,   "Number of blocks");
    ops >> Option('a', "analysis",  analyses,  "Rivet analyses --- can be given multiple times");
    ops >> Option('o', "output",    out_file,  "Output filename.");
    ops >> Option('s', "seed",      seed,      "The Base seed --- this is incremented for each block.");
    ops >> Option('p', "pfile",     pfile,     "Parameter config file for testing __or__ input file name to look for in directory.");
    ops >> Option('i', "indir",     indir,     "Input directory with hierarchical pfiles");
    ops >> Option('H', "h5input",   in_file,   "HDF5 input file");
    if (ops >> Present('h', "help", "Show help"))
    {
        std::cout << "Usage: " << argv[0] << " [OPTIONS]\n";
        std::cout << ops;
        return 1;
    }

    if (!file_exists(in_file)) {
       std::cerr << "HDF5 input file " << in_file << " does not exist, exiting\n";
       exit(1);
    }
    if (!file_exists(pfile)) {
       std::cerr << "Parameter file " << pfile << " does not exist, exiting\n";
       exit(1);
    }

    int nConfigs;
    
    std::vector<std::string> physConfig;
    std::vector<std::vector<std::string> > physConfigs;
    std::vector<std::string> out_files;
    bool f_ok;
    if( world.rank()==0 ) {
       // Program logic: check whether a single parameter file has been given or
       // a directory.
       f_ok = readConfig(pfile, physConfig, verbose);
       if (!f_ok) {
          // Use glob to look for files in directory
          for (auto f : glob(indir + "/*/" + pfile)) {
             physConfig.clear();
             bool this_ok = readConfig(f, physConfig, verbose);
             if (this_ok) {
                physConfigs.push_back(physConfig);
                out_files.push_back(f+".yoda");
             }
          }
       } 
       else {
          physConfigs.push_back(physConfig);
          out_files.push_back(out_file);
       }
       nConfigs=physConfigs.size();
    }

    MPI_Bcast(&nConfigs,   1, MPI_INT, 0, world);



    // ----- starting here is a lot of standard boilerplate code for this kind of
    //       application.
    int mem_blocks  = -1;  // all blocks in memory, if value here then that is how many are in memory
    int dim(1);
    
    size_t blocks;
    if (nBlocks==0) blocks= world.size() * threads;
    else blocks=nBlocks;
    // diy initialization
    diy::FileStorage storage("./DIY.XXXXXX"); // used for blocks moved out of core
    Bounds domain;
    for (int i = 0; i < dim; ++i) {
      domain.min[i] = 0;
      domain.max[i] = blocks-1;
    }
    ////// choice of contiguous or round robin assigner
    diy::ContiguousAssigner   assigner(world.size(), blocks);
    //// decompose the domain into blocks
    //// This is a DIY regular way to assign neighbors. You can do this manually.
    diy::RegularDecomposer<Bounds> decomposer(dim, domain, blocks);

    int k = 2;       // the radix of the k-ary reduction tree
    diy::RegularBroadcastPartners comm(decomposer, k, true);

    diy::RegularMergePartners  partners(decomposer,  // domain decomposition
                                        k,           // radix of k-ary reduction
                                        true); // contiguous = true: distance doubling


    diy::Master master(world, threads, mem_blocks, &Block::create, &Block::destroy,
                     &storage, &Block::save, &Block::load);
    AddBlock create(master);
    decomposer.decompose(world.rank(), assigner, create); // Note: only decompose once!

    if( world.rank()==0 ) {
      fmt::print(stderr, "\n*** This is diy running Pythia8 ***\n");
      fmt::print(stderr, "\n    LHE will be read from {}\n", in_file);
      fmt::print(stderr, "\n    Blocks:                  {}\n", blocks);
      fmt::print(stderr, "\n    Physics configurations:  {}\n", nConfigs);
      fmt::print(stderr, "\n    Number of events/config: {}\n", nEvents);
      fmt::print(stderr, "\n    Total number of events:  {}\n", nEvents*nConfigs);
      if (runmode==1) {
         fmt::print(stderr, "\n  P E R F O R M A N C E  \n");
      }
      if (runmode==2) {
         fmt::print(stderr, "\n  P E R F O R M A N C E S A M E \n");
      }
      fmt::print(stderr, "***********************************\n");
    }

    double starttime, endtime;

    PointConfig pc;
    for (size_t ipc=0;ipc<nConfigs;++ipc) {
       if (world.rank()==0) {
          pc = mkRunConfig(blocks, nEvents, seed, physConfigs[ipc], analyses, out_files[ipc]);
       }

       // We need to tell the first block about the new configuration
       master.foreach([world, pc](Block* b, const diy::Master::ProxyWithLink& cp)
                        {set_pc(b, cp, pc); });

       // Broadcast the runconfig to the other blocks
       diy::reduce(master, assigner, comm, &bc_pointconfig<Block>);

       if (verbose) master.foreach([world](Block* b, const diy::Master::ProxyWithLink& cp)
                        {print_block(b, cp, world.rank()); });

       if (runmode==1) {
         std::streambuf *old = cout.rdbuf();
         stringstream ss;
         cout.rdbuf (ss.rdbuf());
         starttime = MPI_Wtime();
         master.foreach([world, verbose, ipc, in_file, nEvents](Block* b, const diy::Master::ProxyWithLink& cp)
                           {process_block_lhe_performance(b, cp, world.size(), world.rank(), verbose, ipc, in_file, nEvents); });
         endtime   = MPI_Wtime(); 
         cout.rdbuf (old);
         printf("[%i] took %.3f seconds\n",world.rank(), endtime-starttime);
       }
       else if (runmode==2) {
         //std::streambuf *old = cout.rdbuf();
         //stringstream ss;
         //cout.rdbuf (ss.rdbuf());
         starttime = MPI_Wtime();
         master.foreach([world, verbose, ipc, in_file, nEvents](Block* b, const diy::Master::ProxyWithLink& cp)
                           {process_block_lhe_performance_same(b, cp, world.size(), world.rank(), verbose, ipc, in_file, nEvents); });
         endtime   = MPI_Wtime(); 
         //cout.rdbuf (old);
         printf("[%i] took %.3f seconds\n",world.rank(), endtime-starttime);
       }
       else {

	 master.foreach([world, verbose, ipc, in_file, nEvents](Block* b, const diy::Master::ProxyWithLink& cp)
			 {process_block_lhe(b, cp, world.size(), world.rank(), verbose, ipc, in_file, nEvents); });


          diy::reduce(master,              // Master object
                      assigner,            // Assigner object
                      partners,            // RegularMergePartners object
                      &reduceData<Block>);

          //////This is where the write out happens --- the output file name is part of the blocks config
          master.foreach([world, verbose](Block* b, const diy::Master::ProxyWithLink& cp)
                         { write_yoda(b, cp, world.rank(), verbose); });

          // Wipe the buffer to prevent double counting etc.
          master.foreach([world, verbose](Block* b, const diy::Master::ProxyWithLink& cp)
                         { clear_buffer(b, cp, verbose); });
       }
   }


    return 0;
}
