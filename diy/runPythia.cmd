Tune:ee = 7
Beams:frameType = 5

Main:numberOfEvents = 100000
Next:numbercount = 1000000

! Use same PDFs / alpha_s value as in ME calculation
SpaceShower:alphaSvalue = 0.118
TimeShower:alphaSvalue = 0.118
PartonLevel:MPI = Off

! Wimpy shower
TimeShower:pTmaxMatch = 1
SpaceShower:pTmaxMatch = 1

! Specify merging parameters for CKKW-L, UMEPS, UNLOPS.
Merging:TMS = 20
Merging:Process = pp>h
Merging:nJetMax = 1
! Factorisation/renormalisation scales in the 2->1 process
Merging:muFac = 125
Merging:muRen = 125
Merging:muFacInME = 125
Merging:muRenInME = 125
! Switches
Merging:enforceCutOnLHE = on
Merging:applyVeto = off
Merging:includeWeightInXsection = on
Merging:doUserMerging = on
Merging:unorderedASscalePrescrip = 0
Merging:incompleteScalePrescrip = 1
Merging:nJetMinWinnerTakesAll = 7

! Do not include rapidity ordering (not necessary!)
SpaceShower:rapidityOrder = off

! Be more forgiving with momentum mismatches.
Check:epTolErr = 2e-2

Stat:showPartonLevel = true

! disable higgs decays
25:mayDecay = false
